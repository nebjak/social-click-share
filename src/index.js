(function(document) {
  // click listener
  const socialClickShare = event => {
    event.preventDefault();
    const clicker = event.currentTarget;
    const network = clicker.dataset.network;
    openInNewTab(socialUrl(network, document.location, document.title));
  };

  // factory that generates share url based on social network
  const socialUrl = (network, url, text = '') => {
    let outUrl = '';

    switch (network) {
      case 'twitter':
        outUrl = `https://twitter.com/intent/tweet?text=${text}&url=${url}`;
        break;

      case 'facebook':
        outUrl = `https://www.facebook.com/sharer/sharer.php?u=${url}`;
        break;

      case 'linkedin':
        outUrl = `https://www.linkedin.com/shareArticle?mini=true&url=${url}`;
        break;

      default:
        break;
    }

    return encodeURI(outUrl);
  };

  // opens link in new tab
  const openInNewTab = href => {
    Object.assign(document.createElement('a'), {
      target: '_blank',
      href,
    }).click();
  };

  // Adds click event listeners to elements with 'socialClicker' class
  const clickers = document.getElementsByClassName('socialClicker');

  for (const clicker of clickers) {
    clicker.addEventListener('click', socialClickShare);
  }
})(document);
